# React Timer
## Opis
Aplikacja do logowania czasu. Projekt służył do poznania podstaw React.js

## Instalacja produkcyjna
**Pamiętaj o aktualizacji linku w api/index.js**
1. Zbudowanie API, dostępnego pod https://gitlab.com/forgetter/express-timer
2. Zbudowanie produkcyjnej wersji aplikacji `npm run build`
3. Zbudowanie i uruchomienie aplikacji
```docker build -t timer-react:1.0.1 .```
```docker run --rm --network timer-app -d --name timer-react -p 8101:8101 timer-react:1.0.0```

Aplikacja dostępna jest pod adresem localhost:8101

## Instalacja dev
1. Zbudowanie API, dostępnego pod https://gitlab.com/forgetter/express-timer
2. `npm run build` (wymaga zainstalowanego npm)