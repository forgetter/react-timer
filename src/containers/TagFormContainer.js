import { connect } from 'react-redux'
import TagForm from '../components/TagForm'
import { addTagAction } from '../actions/tags';
import { addTag } from '../api/tag'
import { isBlank } from '../utils/form';
import { extractError, newError, clearError } from '../utils/errors';

export const ADD_TAG_ERROR_TYPE = "ADD_TAG_NAME"

const performAddTag = async (tagName, dispatch) => {
  if (isBlank(tagName)) {
    newError(ADD_TAG_ERROR_TYPE, 'Tag name cannot be blank!', dispatch)
    return false
  }
  try {
    let tag = await addTag(tagName)
    dispatch(addTagAction(tag))
    return true
  } catch (e) {
    newError(ADD_TAG_ERROR_TYPE, e.json.error, dispatch)
    return false
  }
}

const mapStateToProps = state => ({
  error: extractError(ADD_TAG_ERROR_TYPE, state)
})

const mapDispatchToProps = dispatch => ({
  addTag: tagName => performAddTag(tagName, dispatch),
  handleError: error => clearError(error.category, dispatch)
});
 
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TagForm);
