import { connect } from 'react-redux'
import Login from '../components/Login'
import { performLogin } from '../api/auth'
import { saveUserAuthData } from '../utils/auth';
import { changeLoginStatus } from '../actions/common';
import { newError, extractError, clearError } from '../utils/errors';

export const LOGIN_ERROR_TYPE = 'LOGIN_ERROR'

const authorize = async (login, password, dispatch) => {
  try {
    let data = await performLogin(login, password)
    saveUserAuthData(data)
    dispatch(changeLoginStatus(true))
  } catch(e) {
    newError(LOGIN_ERROR_TYPE, 'Invalid user or password', dispatch)
    dispatch(changeLoginStatus(false))
  }
}

const mapDispatchToProps = dispatch => ({
  authorize: (login, password) => authorize(login, password, dispatch),
  handleError: error => clearError(error.category, dispatch)
})

const mapStateToProps = state => ({
  isLogged: state.common.isLogged,
  error: extractError(LOGIN_ERROR_TYPE, state)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login)
