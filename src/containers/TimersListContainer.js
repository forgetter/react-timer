import { connect } from 'react-redux'
import TimersList from '../components/TimersList'
import { toInt } from '../utils/common'
import { removeTimer, editTimer } from '../api/timer';
import { newError, clearError } from '../utils/errors';
import { removeTimerAction, editTimerAction } from '../actions/timers';

const TIMER_REMOVE_ERROR = 'TIMER_REMOVE_ERROR'
const TIMER_EDIT_ERROR = 'TIMER_EDIT_ERROR'

const formData = (tags, timers) => (
  timers
    .filter(s => toInt(s.endTime) > 0)
    .sort((lhs, rhs) => rhs.startTime - lhs.startTime)
    .map(s => {
      let tag = tags.find(t => t.id === s.tagId)
      return {
        id: s.id,
        name: tag ? tag.name : "Untagged",
        tag: tag,
        interval: toInt(s.endTime - s.startTime),
        started: toInt(s.startTime)
      }
    })
)

const performEditTimer = async (timer, dispatch) => {
  try {
    let editedTimer = {
      tagId: timer.tag.id,
      startTime: timer.started,
      id: timer.id,
      endTime: toInt(timer.started) + toInt(timer.interval)
    }
    let result = await editTimer(editedTimer)
    dispatch(editTimerAction(result))
    return true
  } catch (e) {
    newError(TIMER_EDIT_ERROR, e.json.error, dispatch)
    return false
  }
}

const mapStateToProps = state => ({
  timers: formData(state.tags, state.timers),
  tags: state.tags
});

const performTimerRemoval = async (timer, dispatch) => {
  try {
    await removeTimer(timer)
    dispatch(removeTimerAction(timer))
  } catch(e) {
    newError(TIMER_REMOVE_ERROR, e.json.error, dispatch)
  }
}

const mapDispatchToProps = dispatch => ({
  removeTimer: timer => performTimerRemoval(timer, dispatch),
  editTimer: timer => performEditTimer(timer, dispatch),
  handleError: error => clearError(error.category, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TimersList)
