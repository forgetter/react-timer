import { connect } from 'react-redux'
import TagsList from '../components/TagsList'
import { removeTagAction, editTagAction } from '../actions/tags'
import { removeTag, editTag } from '../api/tag'
import { newError, clearError, extractError } from '../utils/errors';
import { isBlank, sortAlphabetically } from '../utils/form';

const REMOVE_TAG_ERROR = 'REMOVE_TAG_ERROR'
const EDIT_TAG_ERROR = 'EDIT_TAG_ERROR'

const performRemoval = async (tag, dispatch) => {
  try {
    await removeTag(tag)
    dispatch(removeTagAction(tag))
  } catch (e) {
    newError(REMOVE_TAG_ERROR, e.json.error, dispatch)
  }
}

const performEditTag = async (tag, dispatch) => {
  if (isBlank(tag.name)) {
    newError(EDIT_TAG_ERROR, 'Tag name cannot be blank!', dispatch)
    return false
  }
  try {
    let editedTag = await editTag(tag)
    dispatch(editTagAction(editedTag))
    return true
  } catch (e) {
    newError(EDIT_TAG_ERROR, e.json.error, dispatch)
    return false
  }
}

const mapStateToProps = state => ({
  tags: state.tags.sort((lhs, rhs) => sortAlphabetically(lhs.name, rhs.name)),
  editError: extractError(EDIT_TAG_ERROR, state)
});

const mapDispatchToProps = dispatch => ({
  removeTag: tag => performRemoval(tag, dispatch),
  editTag: tag => performEditTag(tag, dispatch),
  handleError: error => clearError(error.category, dispatch)
});
 
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TagsList)
