import { connect } from 'react-redux'
import TimersForm from '../components/TimersForm'
import { loadTagsAction } from '../actions/tags'
import { loadTags } from '../api/tag'
import { toInt, getUnixTime } from '../utils/common'
import { loadTimers, editTimer, addTimer } from '../api/timer';
import { loadTimerAction, startTimerAction, editTimerAction } from '../actions/timers';
import { newError } from '../utils/errors';

const TAGS_TIMERS_LOAD_ERROR = 'TAGS_TIMERS_LOAD_ERROR'
const TIMER_ADD_ERROR = 'TIMER_ADD_ERROR'
const TIMER_EDIT_ERROR = 'TIMER_EDIT_ERROR'

const prepareLastTag = (timers, tags) => {
  let timer = undefined
  if(timers.length > 0) {
    timer = timers.sort((lhs, rhs) => rhs.startTime - lhs.startTime)[0]
  }
  if(timer) {
    return tags.find(tag => timer.tagId === tag.id)
  }
  return undefined
}

const prepareTimer = (timers, tags) => {
  let timer = timers.find(t => toInt(t.endTime) < 1)
  if(timer) {
    return {
      tag: tags.find(tag => timer.tagId === tag.id),
      startTime: timer.startTime * 1000,
      id: timer.id
    }
  }
  return null
}

const performLoad = async (dispatch) => {
  try {
    let tags = await loadTags()
    let timers = await loadTimers()
    dispatch(loadTagsAction(tags))
    dispatch(loadTimerAction(timers))
  } catch(e) {
    newError(TAGS_TIMERS_LOAD_ERROR, e.json.error, dispatch)
  }
}

const performTimerStop = async (timer, dispatch) => {
  timer.endTime = getUnixTime()
  timer.tagId = timer.tag.id
  try {
    let editedTimer = await editTimer(timer)
    dispatch(editTimerAction(editedTimer))
  } catch(e) {
    newError(TIMER_EDIT_ERROR, e.json.error, dispatch)
  }
}

const performTimerStart = async (tag, dispatch) => {
  let newTimer = {
    tagId: tag.id,
    startTime: getUnixTime()
  }
  try {
    let timer = await addTimer(newTimer)
    dispatch(startTimerAction(timer))
  } catch(e) {
    newError(TIMER_ADD_ERROR, e.json.error, dispatch)
  }
}

const mapDispatchToProps = dispatch => ({
  loadData: () => performLoad(dispatch),
  startTimer: timer => performTimerStart(timer, dispatch),
  stopTimer: timer => performTimerStop(timer, dispatch)
})

const mapStateToProps = state => ({
  tags: state.tags,
  timer: prepareTimer(state.timers, state.tags),
  lastTag: prepareLastTag(state.timers, state.tags)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TimersForm)
