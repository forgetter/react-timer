import { ADD_TAG_ACTION, EDIT_TAG_ACTION, REMOVE_TAG_ACTION, LOAD_TAG_ACTION } from "../actions/tags";

const tags = (state = [], action) => {
  switch (action.type) {
    case ADD_TAG_ACTION:
      return [
        ...state,
        {
          id: action.tag.id,
          name: action.tag.name
        }
      ]
    case EDIT_TAG_ACTION:
      return state.filter(tag => tag.id !== action.tag.id)
        .concat([{
          id: action.tag.id,
          name: action.tag.name
        }])
    case REMOVE_TAG_ACTION:
      return state.filter(tag => tag.id !== action.tag.id)
    case LOAD_TAG_ACTION:
      return action.tags
    default:
      return state
  }
}

export default tags
