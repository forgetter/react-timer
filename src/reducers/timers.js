import { START_TIMER, EDIT_TIMER, REMOVE_TIMER, LOAD_TIMER } from "../actions/timers";

const timers = (state = [], action) => {
  switch (action.type) {
    case START_TIMER:
      return [
        ...state,
        action.timer
      ]
    case EDIT_TIMER:
      return state.map(t => t.id === action.timer.id ? action.timer : t)
    case REMOVE_TIMER:
      return state.filter(t => t.id !== action.id)
    case LOAD_TIMER:
      return action.timers;
    default:
      return state
  }
}

export default timers
