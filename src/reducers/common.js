import { LOGIN_ACTION } from "../actions/common";
import { userLogged } from "../utils/auth";

const initState = {isLogged: userLogged()}

const common = (state = initState, action) => {
  switch (action.type) {
    case LOGIN_ACTION:
      return {isLogged: action.isLogged}
    default:
      return state
  }
}

export default common
