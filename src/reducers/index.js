import { combineReducers } from 'redux'
import tags from './tags'
import errors from './errors'
import timers from './timers'
import common from './common'
 
export default combineReducers({
  tags,
  timers,
  errors,
  common
})
