import { NEW_ERROR_ACTION, HANDLER_ERROR_ACTION } from "../actions/errors";

const errors = (state = [], action) => {
  switch (action.type) {
    case NEW_ERROR_ACTION:
      console.log("New error: " + action.category)
      console.log(action.message)
      return [
        ...state,
        {
          message: action.message,
          category: action.category
        }
      ]
    case HANDLER_ERROR_ACTION:
      return state.filter(e => e.category !== action.category)
    default:
      return state
  }
}

export default errors
