export const twoPlaces = (number) => {
    return ('0' + toInt(number)).slice(-2)
}

// Casts to number. Returns rounded number or 0
export const toInt = (number) => {
    return number | 0
}

export const isEmpty = (str) => {
    return (!str || 0 === str.length);
  }

export const getUnixTime = () => {
    return Math.round(new Date().getTime() / 1000)
}

export const formatDate = (unixDate) => {
    let date = new Date(unixDate * 1000)
    let hours = twoPlaces(date.getHours())
    let minutes = twoPlaces(date.getMinutes())
    let seconds = twoPlaces(date.getSeconds())
    let days = twoPlaces(date.getDate())
    let months = twoPlaces(date.getMonth() + 1)
    return `${hours}:${minutes}:${seconds} ${days}/${months}/${date.getFullYear()}`
}

export const formatSeconds = (unixSeconds) => {
    let seconds = twoPlaces(unixSeconds % 60)
    let minutes = twoPlaces((unixSeconds / 60) % 60)
    let hours = toInt(unixSeconds / 3600)
    return `${hours}:${minutes}:${seconds}`
}
  
export const formatMillis = (millis) => {
    return formatSeconds(toInt(millis / 1000))
}