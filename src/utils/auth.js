import { getUnixTime, toInt } from "./common";

const TOKEN_KEY = 'token'
const EXPIRATION_KEY = 'expires'
const USER_ID = 'userId'

export const getToken = () => {
    return localStorage.getItem(TOKEN_KEY)
}

export const saveUserAuthData = (userAuthData) => {
    localStorage.setItem(TOKEN_KEY, userAuthData.token)
    localStorage.setItem(EXPIRATION_KEY, userAuthData.tokenExpiration)
    localStorage.setItem(USER_ID, userAuthData.userId)
}

export const removeUserData = () => {
    localStorage.removeItem(TOKEN_KEY)
    localStorage.removeItem(EXPIRATION_KEY)
    localStorage.removeItem(USER_ID)
}

export const tokenExpired = () => {
    return toInt(localStorage.getItem(EXPIRATION_KEY)) < getUnixTime()
}

// Should be used only to internal mechanism (like initialise redux)
export const userLogged = () => {
    return !tokenExpired()
}

export const getUserId = () => {
    return localStorage.getItem(USER_ID)
}