import { handleError, addError } from "../actions/errors";

export const extractError = (category, state) => {
    return state.errors.find(e => e.category === category)
}

export const clearError = (category, dispatch) => {
    dispatch(handleError(category))
}

export const newError = (category, message, dispatch) => {
    dispatch(addError(category, message))
}

export const errorMessage = (error) => {
    return error ? error.message : ''
}