export const isBlank = (str) => {
    return (!str || /^\s*$/.test(str));
}

export const sortAlphabetically = (lhs, rhs) => {
    if(lhs < rhs) return -1;
    if(lhs > rhs) return 1;
    return 0;
}