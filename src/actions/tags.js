
export const ADD_TAG_ACTION = 'ADD_TAG'
export const REMOVE_TAG_ACTION = 'REMOVE_TAG'
export const LOAD_TAG_ACTION = 'LOAD_TAGS'
export const EDIT_TAG_ACTION = 'EDIT_TAG'

export const addTagAction = (tag) => ({
    type: ADD_TAG_ACTION,
    tag
})

export const editTagAction = (tag) => ({
    type: EDIT_TAG_ACTION,
    tag
})

export const removeTagAction = (tag) => ({
    type: REMOVE_TAG_ACTION,
    tag
})

export const loadTagsAction = (tags) => ({
    type: LOAD_TAG_ACTION,
    tags
})