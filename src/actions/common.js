export const LOGIN_ACTION = 'CHANGE_LOGIN'

export const changeLoginStatus = (isLogged) => ({
    type: LOGIN_ACTION,
    isLogged
})