export const LOAD_TIMER = 'LOAD_TIMER'
export const START_TIMER = 'START_TIMER'
export const EDIT_TIMER = 'EDIT_TIMER'
export const REMOVE_TIMER = 'REMOVE_TIMER'

export const loadTimerAction = (timers) => ({
    type: LOAD_TIMER,
    timers
})

export const startTimerAction = (timer) => ({
    type: START_TIMER,
    timer
})

export const editTimerAction = (timer) => ({
    type: EDIT_TIMER,
    timer
})

export const removeTimerAction = (timer) => ({
    type: REMOVE_TIMER,
    id: timer.id
})