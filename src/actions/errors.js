export const NEW_ERROR_ACTION = 'NEW_ERROR'
export const HANDLER_ERROR_ACTION = 'HANDLE_ERROR'

export const addError = (category, message) => ({
    type: NEW_ERROR_ACTION,
    category,
    message
})

export const handleError = (category) => ({
    type: HANDLER_ERROR_ACTION,
    category
})