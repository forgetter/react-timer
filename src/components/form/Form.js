import React from 'react'
import { isEmpty } from '../../utils/common';

export const Form = ({ children }) => (<form className="content">{children}</form>)

const addLabelIfPresent = (label) => {
    return isEmpty(label) ? null : <label className="label">{label}</label>
}

const addErrorInfoIfPresent = (error) => {
    return isEmpty(error) ? null : <p className="help is-danger">{error}</p>
}

const determineInputClass = ({error}) => {
    let className = "input"
    if(!isEmpty(error)) {
        className += " is-danger"
    }
    return className
}

export const FormField = (props) => (
    <div className="field">
        {addLabelIfPresent(props.label)}
        <div className="control">
            <input className={determineInputClass(props)} 
                type={props.type || "text"}
                placeholder={props.placeholder || props.label || ''} 
                onChange={e => props.onChange(e.target.value)}
                value={props.value} />
            {addErrorInfoIfPresent(props.error)}
        </div>
    </div>
)

export const FormGroup = ({children}) => (
    <div className="field is-grouped is-grouped-right">{children}</div>
)

const determineButtonClass = ({isDanger, isLoading, isSuccess, isText}) => {
    let className = "button"
    if(isText) {
        className += " is-text"
    }
    if(isDanger) {
        className += " is-danger"
    } else if(isSuccess) {
        className += " is-success"
    } else if(!isText) {
        className += " is-info"
    }
    if(isLoading) {
        className += " is-loading"
    }
    return className
}

export const FormButton = (props) => (
    <div className="control" onClick={!props.isLoading ? props.onClick : null}>
        <button className={determineButtonClass(props)}>{props.label}</button>
    </div>
)
