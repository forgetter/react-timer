import { connect } from 'react-redux'

const _UserContent = ({children, isLogged}) => {return isLogged ? children : null}
const _GuestContent = ({children, isLogged}) => { return !isLogged ? children : null}

export const UserContent = connect(state => ({isLogged: state.common.isLogged}), null)(_UserContent)
export const GuestContent = connect(state => ({isLogged: state.common.isLogged}), null)(_GuestContent)

