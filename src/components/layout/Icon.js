import React from 'react'
import PropTypes from 'prop-types';

function determineIconClass(props) {
    let cls = "icon"
    if(props.isDanger) {
        cls += " has-text-danger"
    } else if(props.isInfo) {
        cls += " has-text-info"
    }
    return cls
}

const Icon = (props) => (
    <span className={determineIconClass(props)} onClick={props.onClick}>
        <i className={props.name}></i>
    </span>
)

Icon.propTypes = {
    name: PropTypes.string.isRequired,
    danger: PropTypes.bool,
    onClick: PropTypes.func,
}

export default Icon
