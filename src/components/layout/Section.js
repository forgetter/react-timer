import React from 'react'
import PropTypes from 'prop-types'
import Box from './Box';

const Section = ({ children, title }) => (
    <section className="section">
        <div className="container">
            <h1 className="title">{title}</h1>
            <Box>{children}</Box>
        </div>
    </section>
)

Section.propTypes = {
    children: PropTypes.object,
    title: PropTypes.string
  }

export default Section