import React from 'react'

const Content = (props) => (
  <div className="site-content">
		{props.children}
  </div>
)

export default Content
