import React from 'react'

const Footer = () => (
  <footer className="footer">
    <div className="container">
      <div className="content has-text-left">

        <p>
          Created by <strong>Mikołaj Styś</strong>
        </p>
        <p>
          Created using <strong><i className="fab fa-react"/> ReactJS</strong><br/>
          <a href="https://bulma.io">
            <img src="https://bulma.io/images/made-with-bulma.png" alt="Made with Bulma" width="128" height="24"/>
          </a>
        </p>
      </div>
    </div>
  </footer>
)

export default Footer
