import React from 'react'
import PropTypes from 'prop-types';

class Select extends React.Component {

    render() {
        return (
            <div className="select">
                <select value={this.props.selected && this.props.selected.id}
                    onChange={(e) => {
                        let selectedId = e.target.value
                        let selectedItem = this.props.items.find(item => item.id === selectedId)
                        this.props.onChange(selectedItem)
                    }}>
                    {this.props.items.map((item) => <option key={item.id} value={item.id}>{item.name}</option>)}
                </select>
            </div>
        )
    }
}

Select.propTypes = {
    items: PropTypes.array,
    onChange: PropTypes.func,
    defaultSelected: PropTypes.object
}

export default Select
