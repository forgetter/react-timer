import React from 'react'
import PropTypes from 'prop-types';

const headerCreator = (item) => (
    <th key={item}>{item}</th>
)

const Table = (props) => (
    <div className="content table-wrapper">
      <table className="table">
        <thead>
          <tr>{ props.keys.map(title => headerCreator(title)) }</tr>
        </thead>
        <tbody>
            { props.items.map(item => props.rowCreator(item)) }
        </tbody>
      </table>
    </div>
)

Table.propTypes = {
    keys: PropTypes.arrayOf(PropTypes.string),
    rowCreator: PropTypes.func,
    items: PropTypes.array,
}

export default Table
