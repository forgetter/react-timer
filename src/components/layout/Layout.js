import React from 'react'

import Site from './Site'
import Header from './Header'
import Content from './Content'
import Footer from './Footer'
import Router from '../Router'

const Layout = () => (
  <Site>
    <Header />
    <Content>
      <Router />
    </Content>
    <Footer />
  </Site>
)

export default Layout
