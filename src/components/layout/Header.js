import React from 'react'
import { NavLink } from 'react-router-dom'
import { UserContent, GuestContent } from './LoggedContent'

const NavTitle = () => (
  <NavLink className="navbar-item" to="/" exact activeClassName="is-active">
    <span className="icon is-medium fa-lg has-text-info">
      <i className="fab fa-react"></i>
    </span>
    React Timer
  </NavLink>
)

const NavBurger = ({ onClick }) => (
  <button className="button navbar-burger" onClick={onClick}>
    <span></span>
    <span></span>
    <span></span>
  </button>
)

const NavButton = ({ title, to, icon }) => (
  <NavLink className="navbar-item" to={to} activeClassName="is-active">
    <span className="icon has-text-primary" style={{ marginRight: 5 }}>
      <i className={icon}/>
    </span>
    {title}
  </NavLink>
)

class Header extends React.Component {

  state = {
    isActive: false,
  }

  toggleNav = () => {
    this.setState(prevState => ({
      isActive: !prevState.isActive
    }))
  }

  render() {
    return (
      <nav className="navbar is-purple has-shadow">
        <div className="navbar-brand">
          <NavTitle />
          <NavBurger onClick={this.toggleNav} />
        </div>
        <div className={this.state.isActive ? 'navbar-menu is-active' : 'navbar-menu'}>
          <div className="navbar-start">
            <GuestContent>
              <NavButton title="Login" to="/login" icon="fas fa-sign-in-alt"/>
            </GuestContent>
            <UserContent>
              <NavButton title="Timers" to="/timers" icon="fas fa-clock"/>
              <NavButton title="Tags" to="/tags" icon="fas fa-tag"/>
            </UserContent>
          </div>
          <div className="navbar-end">
            <UserContent>
              <NavButton title="Logout" to="/logout" icon="fas fa-sign-out-alt"/>
            </UserContent>
          </div>
        </div>
      </nav>
    )
  }
}

export default Header
