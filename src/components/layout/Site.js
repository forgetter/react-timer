import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { AUTH_ERROR_TYPE } from '../../api/auth';
import { extractError, clearError } from '../../utils/errors';

const renderSite = (props) => (
  <div className="site">
		{props.children}
  </div>
)

const Site = (props) => {
    if(props.error) {
      props.handleError()
      return <Redirect to='/logout'/>
    } else {
      return renderSite(props)
    } 
}

const mapDispatchToProps = dispatch => ({
  handleError: () => clearError(AUTH_ERROR_TYPE, dispatch)
})

const mapStateToProps = state => ({
  error: extractError(AUTH_ERROR_TYPE, state)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Site)
