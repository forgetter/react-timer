import React from 'react'

const Box = (props) => (
  <div className="box">{props.children}</div>
)

export default Box
