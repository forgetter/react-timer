import React from 'react'
import { Form, FormField, FormGroup, FormButton } from './form/Form';
import Section from './layout/Section';

class TagForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      tagName: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    this.setState({ loading: true, error: null })
    event.preventDefault();
    this.props.addTag(this.state.tagName)
      .then(success => {
        this.setState({ 
          loading: false, 
          tagName: success ? '' : this.state.tagName,
          success: success
        })
      })
  }

  componentDidUpdate() {
    if(this.props.error) {
      this.setState({error: this.props.error.message})
      this.props.handleError(this.props.error)
    }
  }

  render() {
    return (
      <Section title="Create tag">
        <Form>
          <FormField
            error={this.state.error}
            placeholder="Tag name"
            value={this.state.tagName}
            onChange={tagName => this.setState({ tagName })}
          />
          <FormGroup>
            <FormButton isDanger={this.state.error} isLoading={this.state.loading}
              label="Send" onClick={this.handleSubmit} isSuccess={this.state.success} />
          </FormGroup>
        </Form>
      </Section>
    );
  }
}

export default TagForm;
