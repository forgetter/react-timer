import React from 'react'
import PropTypes from 'prop-types';
import { formatDate, formatSeconds } from '../utils/common'

import Table from './layout/Table'
import Icon from './layout/Icon'
import Section from './layout/Section';
import Select from './layout/Select';
import { extractError } from '../utils/errors';

const tableHeaders = ["Tag", "Start", "Duration", "Options"]

const rowCreator = (timer, removeAction, editAction) => (
  <tr key={timer.id}>
    <td>{timer.name}</td>
    <td>{formatDate(timer.started)}</td>
    <td>{formatSeconds(timer.interval)}</td>
    <td>
      <Icon name="fas fa-trash" isInfo onClick={() => removeAction(timer)} />
      <Icon name="fas fa-edit" isInfo onClick={() => editAction(timer)} />
    </td>
  </tr>
)


class TimersList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      editedTimer: null,
      error: null
    }
    this.editRowCreator = this.editRowCreator.bind(this)
    this.handleEdit = this.handleEdit.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
  }

  handleSubmit() {
    this.props.editTimer(this.state.editedTimer)
    this.handleCancel()
  }

  handleCancel() {
    this.setState({editedTimer: null})
  }

  handleEdit(timer) {
    this.setState({editedTimer: {
      id: timer.id,
      interval: timer.interval,
      started: timer.started,
      tag: timer.tag,
      name: timer.name
    }})
  }

  editRowCreator() {
    return (
      <tr key={this.getEditTimerId()}>
        <td> {/* TODO: error is not shown */}
          <Select selected={this.state.editedTimer.tag}
            items={this.props.tags}
            onChange={tag => {
              let timer = this.state.editedTimer
              timer.tag = tag
              this.setState({ editedTimer: timer })
            }} />
        </td>
        <td>{formatDate(this.state.editedTimer.started)}</td>
        <td>{formatSeconds(this.state.editedTimer.interval)}</td>
        <td key={Math.random()}> {/* Used to force refresh icons */}
          <Icon name="fas fa-check" isInfo onClick={this.handleSubmit} />
          <Icon name="fas fa-times" isInfo onClick={this.handleCancel} />
        </td>
      </tr>
    )
  }

  componentDidUpdate() {
    if(this.props.error) {
      this.setState({error: extractError(this.props.error)})
      this.props.handleError(this.props.error)
    }
  }

  getEditTimerId() {
    return this.state.editedTimer ? this.state.editedTimer.id : null
  }

  render() {
    return (
      <Section title="Timers">
        <Table
          rowCreator={(timer) => {
              if(timer.id === this.getEditTimerId()) {
                return this.editRowCreator()
              } else {
                return rowCreator(timer, this.props.removeTimer, this.handleEdit)
              }
            }}
          items={this.props.timers}
          keys={tableHeaders}
        />
      </Section>
    )
  }
}

TimersList.propTypes = {
  timers: PropTypes.array,
  removeAction: PropTypes.func
}

export default TimersList