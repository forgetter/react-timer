import React from 'react'
import PropTypes from 'prop-types'
import { formatMillis } from '../utils/common'

import Section from './layout/Section'
import Select from './layout/Select'

class TimersForm extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            elapsed: 0
        }
        this.incrementer = null
        this.handleButtonClick = this.handleButtonClick.bind(this)
    }

    componentDidMount() {
        this.props.loadData()
    }

    componentWillUnmount() {
        if (this.incrementer) {
            clearInterval(this.incrementer)
            this.incrementer = null
        }
    }

    componentDidUpdate() {
        if (this.isActive()) {
            if (this.incrementer === null) {
                let timer = this.props.timer
                this.incrementer = setInterval(() => {
                    this.setState({
                        elapsed: new Date().getTime() - timer.startTime
                    })
                }, 500)
                this.setState({
                    elapsed: new Date().getTime() - timer.startTime,
                })
            }
        } else if (this.incrementer !== null) {
            clearInterval(this.incrementer)
            this.incrementer = null
            this.setState({ elapsed: 0 })
        }
    }

    handleButtonClick() {
        this.isActive() 
            ? this.props.stopTimer(this.props.timer)
            : this.props.startTimer(this.determineTag())
    }

    isActive() {
        return this.props.timer || false
    }

    determineTag() {
        if(this.state.selectedTag) {
            return this.state.selectedTag
        } else {
            return this.props.lastTag
        }
    }

    render() {
        return (
            <Section title="Stopwatch">
                <div className="content has-text-centered">
                    <p className="title is-1">{formatMillis(this.state.elapsed)}</p>
                    <Select selected={this.determineTag()}
                        items={this.props.tags}
                        onChange={tag => this.setState({ selectedTag: tag })} />
                    <button style={{ marginLeft: "10px" }}
                        className={"button is-uppercase " + (this.isActive() ? "is-danger" : "is-primary")}
                        onClick={this.handleButtonClick}
                    >
                        {this.isActive() ? "Stop" : "Start"}
                    </button>
                </div>
            </Section>
        );
    }
}

TimersForm.propTypes = {
    tags: PropTypes.array,
    timer: PropTypes.object,
    lastTag: PropTypes.object
}

export default TimersForm