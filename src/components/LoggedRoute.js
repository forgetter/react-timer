import React from 'react'
import { connect } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'

const LoggedRoute = ({isLogged, component, path}) => {
  return isLogged
    ? <Route path={path} component={component}/>
    : <Redirect push from={path} to='/login' />
}

export default connect(state => ({isLogged: state.common.isLogged}), null)(LoggedRoute)
