import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import Timers from './Timers'
import Tags from './Tags'
import Login from '../containers/AuthContainer'
import Section from './layout/Section';
import LoggedRoute from './LoggedRoute';
import Logout from './Logout';

const Router = () => (
  <Switch>
    <Route exact path='/' component={Home}/>
    <LoggedRoute path='/timers' component={Timers}/>
    <LoggedRoute path='/tags' component={Tags}/>
    <Route path='/login' component={Login}/>
    <LoggedRoute path='/logout' component={Logout}/>
    <Route component={NotFound}/>
  </Switch>
)

const NotFound = () => (
  <Section title="Nie znaleziono strony"><p>Stary, gdzie ja jestem?</p></Section>
)

export default Router
