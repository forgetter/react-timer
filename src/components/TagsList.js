import React from 'react'
import Icon from './layout/Icon'
import Table from './layout/Table';
import Section from './layout/Section';
import { FormField } from './form/Form';
import { extractError } from '../utils/errors';

const tagTableTitles = ["Name", "Options"];

const rowCreator = (tag, removeTag, editHandler) => (
  <tr key={tag.id}>
    <td>{tag.name}</td>
    <td>
      <Icon name="fas fa-trash" isInfo onClick={() => removeTag(tag)} />
      <Icon name="fas fa-edit" isInfo onClick={() => editHandler(tag)} />
    </td>
  </tr>
)

class TagsList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      editedTag: null,
      error: null
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleEdit = this.handleEdit.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
    this.editRowCreator = this.editRowCreator.bind(this)
    this.getEditTagId = this.getEditTagId.bind(this)
  }

  handleSubmit() {
    this.props.editTag(this.state.editedTag)
    this.handleCancel()
  }

  handleEdit(tag) {
    this.setState({editedTag: {
      id: tag.id,
      name: tag.name
    }})
  }

  handleCancel() {
    this.setState({editedTag: null})
  }

  editRowCreator() {
    return (
      <tr key={this.state.editedTag.id}>
        <td> {/* TODO: error is not shown */}
          <FormField
            error={this.state.error} 
            placeholder="Tag name"
            value={this.state.editedTag.name}
            onChange={tagName => {
              let editedTag = this.state.editedTag
              editedTag.name = tagName
              this.setState({editedTag})
            }}  
          />
        </td>
        <td key={Math.random()}> {/* Used to force refresh icons */}
          <Icon name="fas fa-check" isInfo onClick={this.handleSubmit} />
          <Icon name="fas fa-times" isInfo onClick={this.handleCancel} />
        </td>
      </tr>
    )
  }

  componentDidUpdate() {
    if(this.props.error) {
      this.setState({error: extractError(this.props.error)})
      this.props.handleError(this.props.error)
    }
  }

  getEditTagId() {
    return this.state.editedTag ? this.state.editedTag.id : null
  }

  render() {
    return (
      <Section title="Available tags">
        <Table
          rowCreator={(tag) => {
              if(tag.id === this.getEditTagId()) {
                return this.editRowCreator()
              } else {
                return rowCreator(tag, this.props.removeTag, this.handleEdit)
              }
            }}
          items={this.props.tags}
          keys={tagTableTitles}
        />
      </Section>
    )
  }
}

export default TagsList;
