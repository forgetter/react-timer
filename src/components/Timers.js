import React from 'react'

import TimersList from '../containers/TimersListContainer';
import TimersForm from '../containers/TimersFormContainer';

const Timers = () => (
  <div>
    <TimersForm />
    <TimersList />
  </div>
)

export default Timers
