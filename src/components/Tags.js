import React from 'react'
import { connect } from 'react-redux'
import TagsList from '../containers/TagsListContainer'
import TagForm from '../containers/TagFormContainer'
import { loadTags } from '../api/tag'
import { loadTagsAction } from '../actions/tags';

const mapDispatchToProps = dispatch => ({
  tagsLoaded: (tags) => dispatch(loadTagsAction(tags))
});

const showLoading = () => (
  <section className="hero is-fullheight">
    <div className="hero-body">
      <div className="container has-text-centered">
        <h1 className="title">Ładowanie</h1>
        <span className="icon is-large">
          <i className="fas fa-spinner fa-spin fa-3x"></i>
        </span>
      </div>
    </div>
  </section>
);

const showError = (error) => (
  <section className="section">
    <div className="container">
      <div className="notification is-danger ">
        Couldn't fetch tags :(
      </div>
    </div>
  </section>
);

const showTags = () => (
  <div>
    <TagForm />
    <TagsList />
  </div>
);

class Tags extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      error: null
    };
  }

  async loadData() {
    let tags = await loadTags()
    this.props.tagsLoaded(tags)
    this.setState({ loading: false })
  }

  componentWillMount() {
    this.loadData()
  }

  render() {
    if (this.state.loading) {
      return showLoading();
    } else if (this.state.error !== null) {
      return showError(this.state.error);
    } else {
      return showTags();
    }
  }
}

export default connect(
  null,
  mapDispatchToProps,
)(Tags);
