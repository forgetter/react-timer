import React from 'react'
import { connect } from 'react-redux'
import Section from './layout/Section';

const mapStateToProps = state => ({
  isLogged: state.common.isLogged
})

const mapDispatchToProps = dispatch => ({
})

const Home = ({isLogged, logout}) => {
  if(!isLogged) {
    return <Section title="Hello unknown user!" />
  } else {
    return (
      <Section title="Hello logged user!"></Section>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home)
