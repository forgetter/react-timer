import React from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { removeUserData } from '../utils/auth';
import { changeLoginStatus } from '../actions/common';

export const Logout = ({logoutAction}) => {
  logoutAction()
  return <Redirect to='/'/>
}

const logoutAction = (dispatch) => {
  removeUserData()
  dispatch(changeLoginStatus(false))
}

const mapDispatchToProps = dispatch => ({
  logoutAction: () => logoutAction(dispatch)
})

const mapStateToProps = state => ({
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Logout)
