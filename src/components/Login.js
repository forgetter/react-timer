import React from 'react'
import Section from './layout/Section'
import { Redirect } from 'react-router-dom'
import { Form, FormField, FormGroup, FormButton } from './form/Form';

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: '',
      loading: false
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(event) {
    event.preventDefault()
    this.setState({loading: true})
    this.props.authorize(this.state.login, this.state.password)
  }

  componentDidUpdate(prevProps) {
    if(!prevProps.isLogged && this.props.isLogged) {
      this.setState({login:'', password: '', loading: false})
      this.props.history.goBack()
    }
    if(this.props.error) {
      this.setState({error: this.props.error.message, loading: false})
      this.props.handleError(this.props.error)
    }
  }

  render() {
    if(this.props.isLogged) {
      return <Redirect to='/'/>
    }
    return (
      <Section title="Login">
        <Form>
          <FormField label="Login" 
            error={this.state.error}
            onChange={(val) => { this.setState({login: val}) }}
          />
          <FormField label="Password" 
            error={this.state.error}
            type="password"
            onChange={(val) => { this.setState({password: val}) }}
          />
          <FormGroup>
            <FormButton isDanger={this.state.error} isLoading={this.state.loading} 
              label="Login" onClick={this.handleSubmit} />
          </FormGroup>
        </Form>
      </Section>
    )
  }
}

export default Login
