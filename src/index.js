import ReactDOM from 'react-dom'
import registerServiceWorker from './registerServiceWorker'
import { BrowserRouter as Router } from 'react-router-dom'
import { Provider } from 'react-redux'
import React from 'react';
import { App } from './components/App'
import './index.css'
import reduxStore from './reduxStore';

ReactDOM.render((
  <Router>
    <Provider store={reduxStore}>
      <App />
    </Provider>
  </Router>
), document.getElementById('root'))

registerServiceWorker()
