import {httpPost, httpGet, httpDelete, httpPut, ApiError} from '.'
import { userLogged, getToken } from '../utils/auth';
import reduxStore from '../reduxStore';
import { newError } from '../utils/errors';

export const AUTH_ERROR_TYPE = 'AUTH_ERROR_TYPE'

const appendToken = (headers) => {
    if(userLogged()) {
        headers.set('Authorization', `Bearer ${getToken()}`)
    }
    return headers
}

const catchApiError = (promise) => {
    return promise
        .catch(e => {
            console.log("caught something!")
            if(e instanceof ApiError && e.status === 401) {
                newError(AUTH_ERROR_TYPE, e.json.error, reduxStore.dispatch)
            } else {
                throw e
            }
        })
}

export const performLogin = async (login, password) => {
    return await httpPost('/login', {login, password})
}

export function httpAuthGet(endpoint, headers = new Headers()) {
    return catchApiError(httpGet(endpoint, appendToken(headers)))
};

export function httpAuthPost(endpoint, json, headers = new Headers()) {
    return catchApiError(httpPost(endpoint, json, appendToken(headers)))
};

export function httpAuthPut(endpoint, json, headers = new Headers()) {
    return catchApiError(httpPut(endpoint, json, appendToken(headers)))
};

export function httpAuthDelete(endpoint, headers = new Headers()) {
    return catchApiError(httpDelete(endpoint, appendToken(headers)))
};