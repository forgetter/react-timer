import { httpAuthPut, httpAuthDelete, httpAuthGet, httpAuthPost } from './auth';

export const loadTimers = () => {
    return httpAuthGet('/timers')
}

export const removeTimer = (timer) => {
    return httpAuthDelete(`/timer/${timer.id}`)
}

export const addTimer = (timer) => {
    return httpAuthPut('/timer', {tagId: timer.tagId, startTime: timer.startTime})
}

export const editTimer = (timer) => {
    return httpAuthPost(`/timer`, {endTime: timer.endTime, tagId: timer.tagId, id: timer.id})
}
