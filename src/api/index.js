const apiUrl = "https://localhost:8102";
const responseOk = 200
const responseHead = 204

//FIXME: dont know why extending Error make it useless
class NetworkError {
    constructor(message) {
        this.message = message
    }
 }

export class ApiError { 

    constructor(response) {
        this.status = response.status
        this.json = response.json
    }
}

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON, status from the response
 */
async function parseJson(response) {
    let json = {}
    if(response.status !== responseHead) {
        json = await response.json()
    }
    return {status: response.status, json}
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {Promise}           The request promise
 */
async function request(url, options) {
    let httpResponse = null
    try {
        httpResponse = await fetch(apiUrl + url, options)
    } catch(error) {
        throw new NetworkError(error.message) 
    }
    let response = await parseJson(httpResponse)
    if (response.status === responseOk || response.status === responseHead) {
        return response.json
    } else {
        throw new ApiError(response)
    }
}

export function httpGet(endpoint, headers = new Headers()) {
    headers.set('Accept', 'application/json')
    return request(endpoint, {
        method: 'GET',
        headers
    })
};

export function httpPost(endpoint, json, headers = new Headers()) {
    headers.set('Accept', 'application/json')
    headers.set('Content-Type', 'application/json')
    return request(endpoint, {
        method: 'POST',
        headers,
        body: JSON.stringify(json)
    })
};

export function httpPut(endpoint, json, headers = new Headers()) {
    headers.set('Accept', 'application/json')
    headers.set('Content-Type', 'application/json')
    return request(endpoint, {
        method: 'PUT',
        headers,
        body: JSON.stringify(json)
    })
};

export function httpDelete(endpoint, headers = new Headers()) {
    headers.set('Accept', 'application/json')
    return request(endpoint, {
        method: 'DELETE',
        headers
    })
};





