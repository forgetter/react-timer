import {httpAuthDelete, httpAuthGet, httpAuthPost, httpAuthPut} from './auth'

export const loadTags = () => {
    return httpAuthGet('/tags')
}

export const removeTag = (tag) => {
    return httpAuthDelete(`/tag/${tag.id}`)
};

export const addTag = (name) => {
    return httpAuthPut('/tag', {name})
}

export const editTag = (tag) => {
    return httpAuthPost('/tag', tag)
}
