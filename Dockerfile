FROM node:alpine

# Create app dir
WORKDIR /usr/src/app

RUN npm i -g --only=production serve

COPY ./build/ ./

EXPOSE 8101/tcp 
# Bundle app
CMD [ "serve",  "-s", "-p", "8101", "./"]